<?php

namespace RjgcPay\AliAppPay\buildermodel;



/* *
 * 功能：支付宝电脑网站alipay.trade.close (统一查询对账单下载地址)业务参数封装
 * 版本：2.0
 * 修改日期：2017-05-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 */


class AlipayDataDataserviceBillDownloadurlQueryBuilder
{

    // 账单类型.
    private $bill_type;

    // 账单时间
    private $bill_date;

    private $bizContentarr = array();

    private $bizContent = NULL;

    public function getBizContent()
    {
        if(!empty($this->bizContentarr)){
            $this->bizContent = json_encode($this->bizContentarr,JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    public function getBillType()
    {
        return $this->bill_type;
    }

    public function setBillType($bill_type)
    {
        $this->bill_type = $bill_type;
        $this->bizContentarr['bill_type'] = $bill_type;
    }

    public function getBillDate()
    {
        return $this->bill_date;
    }

    public function setBillDate($outTradeNo)
    {
        $this->bill_date = $outTradeNo;
        $this->bizContentarr['bill_date'] = $outTradeNo;
    }

}

?>