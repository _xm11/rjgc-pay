<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\WxMiniPay;

use RjgcPay\PayModeInterface;


/**
 * 微信支付处理类-小程序支付
 *
 * @package RjgcPay\WxMiniPay
 */
class PayMode implements PayModeInterface
{
    /**
     * 支付构造函数
     * PayMode constructor.
     * @param $app_config  自定义配置信息(外部)
     * @param $payMode_config  特定支付配置信息（内部）
     * @throws \Exception
     */
    public function __construct($app_config,$payMode_config)
    {
        require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Api.php';
        new Pay($app_config,$payMode_config);

    }


    /**
     * 统一付款接口
     *
     * @param $data  支付需要提供的参数
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedPay($data)
    {
        print_r('开发中');die;
        return $result;
    }


    /**
     * 统一同步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台同步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnUrl($data)
    {
        print_r('开发中');die;
        $this->writeLog(json_encode($data,JSON_UNESCAPED_UNICODE));
        return $this->check($data);
    }

    /**
     * 统一异步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台异步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnNotify($data)
    {
        print_r('开发中');die;
        $this->writeLog(json_encode($data,JSON_UNESCAPED_UNICODE));
        return $this->check($data);
    }

    /**
     * 统一收单线下交易查询.
     *
     * @param \RjgcPay\查询条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本\
     */
    public function unifiedQuery($data)
    {
        print_r('开发中');die;
        return $response;
    }


    /**
     * 统一收单交易关闭接口.
     *
     * @param \RjgcPay\关闭条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedClose($data)
    {
        print_r('开发中');die;
        return $response;
    }

    /**
     * 统一收单交易退款接口.
     *
     * @param \RjgcPay\退款参数 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedRefund($data)
    {
        print_r('开发中');die;
        return $response;
    }


    /**
     * 统一收单交易退款查询.
     *
     * @param \RjgcPay\退款查询条件 $data
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedRefundQuery($data)
    {
        print_r('开发中');die;
        return $response;
    }


    public function unifiedDownloadurlQuery($data)
    {
        print_r('尚未实现');
        // TODO: Implement unifiedDownloadurlQuery() method.
    }

    /**
     * 验签方法
     *
     * @param $data 验签支付宝返回的信息，使用支付宝公钥
     */
    public function check($data)
    {
        print_r('开发中');die;
        return $result;
    }

    /**
     * 写入日志.
     *
     * @param $text
     */
    public function writeLog($text)
    {
        print_r('开发中');die;
        file_put_contents ( dirname ( __FILE__ ).DIRECTORY_SEPARATOR."./../../log.txt", date ( "Y-m-d H:i:s" ) . "  " . $text . "\r\n", FILE_APPEND );
    }


    private function _execute($request){
        print_r('开发中');die;
        return $result;
    }

}