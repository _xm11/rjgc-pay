<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\WxMiniPay;

/**
 * 微信支付类-小程序支付
 *
 * Class Pay
 * @package RjgcPay\WxMiniPay
 */
class Pay
{
    /**
     * Pay constructor.
     * @param $app_config  自定义配置信息(外部)
     * @param $payMode_config 特定支付配置信息（内部）
     * @throws \Exception
     */
    function __construct($app_config,$payMode_config){
        if (!empty($app_config)){

            if (empty($app_config['wechat_appid']) || trim($app_config['wechat_appid']) == "") {
                throw new \Exception("绑定支付的APPID必填(使用外部配置)");
            } else {
                \WxPayConfig::$APPID = $app_config['wechat_appid'];  //绑定支付的APPID（必须配置，开户邮件中可查看）
            }

            if (empty($app_config['wechat_appsecret']) || trim($app_config['wechat_appsecret']) == "") {
                throw new \Exception("公众帐号secert必填(使用外部配置)");
            } else {
                \WxPayConfig::$APPSECRET = $app_config['wechat_appsecret'];  //公众帐号secert（仅JSAPI支付的时候需要配置， 登录公众平台，进入开发者中心可设置）
            }

            if (empty($app_config['payment']['merchant_id']) || trim($app_config['payment']['merchant_id']) == "") {
                throw new \Exception("商户号必填(使用外部配置)");
            } else {
                \WxPayConfig::$MCHID = $app_config['payment']['merchant_id'];  //商户号（必须配置，开户邮件中可查看）
            }

            if (empty($app_config['payment']['key']) || trim($app_config['payment']['key']) == "") {
                throw new \Exception("商户支付密钥必填(使用外部配置)");
            } else {
                \WxPayConfig::$KEY = $app_config['payment']['key'];  //商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）
            }


            if (empty($app_config['payment']['mini_pay_notify_url']) || trim($app_config['payment']['mini_pay_notify_url']) == "") {
                if (empty($app_config['payment']['notify_url']) || trim($app_config['payment']['notify_url']) == "") {
                    throw new \Exception("商户支付回调地址必填(使用外部配置)");
                } else {
                    \WxPayConfig::$NOTIFY_URL = $app_config['payment']['notify_url'];  //
                }
            } else {
                \WxPayConfig::$NOTIFY_URL = $app_config['payment']['pc_pay_notify_url_two'];  //
            }

            \WxPayConfig::$SSLCERT_PATH = $app_config['payment']['cert_path'];  //
            \WxPayConfig::$SSLKEY_PATH = $app_config['payment']['key_path'];  //

        }
        elseif (!empty($payMode_config)){
            if (empty($payMode_config['appid']) || trim($payMode_config['appid']) == "") {
                throw new \Exception("绑定支付的APPID必填(使用内部配置)");
            } else {
                \WxPayConfig::$APPID=  $payMode_config['appid'];  //绑定支付的APPID（必须配置，开户邮件中可查看）
            }

            if (empty($payMode_config['appsecret']) || trim($payMode_config['appsecret']) == "") {
                throw new \Exception("公众帐号secert必填(使用内部配置)");
            } else {
                \WxPayConfig::$APPSECRET=  $payMode_config['appsecret'];  //公众帐号secert（仅JSAPI支付的时候需要配置， 登录公众平台，进入开发者中心可设置）
            }

            if (empty($payMode_config['mchid']) || trim($payMode_config['mchid']) == "") {
                throw new \Exception("商户号必填(使用内部配置)");
            } else {
                \WxPayConfig::$MCHID=  $payMode_config['mchid'];  //商户号（必须配置，开户邮件中可查看）
            }

            if (empty($payMode_config['key']) || trim($payMode_config['key']) == "") {
                throw new \Exception("商户支付密钥必填(使用内部配置)");
            } else {
                \WxPayConfig::$KEY=  $payMode_config['key'];  //商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）
            }

            if (empty($payMode_config['notify_url']) || trim($payMode_config['notify_url']) == "") {
                throw new \Exception("商户支付回调地址必填必填(使用内部配置)");
            } else {
                \WxPayConfig::$NOTIFY_URL=  $payMode_config['notify_url'];  //
            }

            \WxPayConfig::$SSLCERT_PATH=  $payMode_config['sslcert_path'];  //
            \WxPayConfig::$SSLKEY_PATH=  $payMode_config['sslkey_path'];  //
        }

        else {
            throw new \Exception('请先进行支付配置');
        }
    }
}