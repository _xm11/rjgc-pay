<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\WxH5Pay;

use RjgcPay\PayModeInterface;


/**
 * 微信支付处理类-H5页面支付
 *
 * @package RjgcPay\WxH5Pay
 */
class PayMode implements PayModeInterface
{
    /**
     * 支付构造函数
     * PayMode constructor.
     * @param $app_config  自定义配置信息(外部)
     * @param $payMode_config  特定支付配置信息（内部）
     * @throws \Exception
     */
    public function __construct($app_config,$payMode_config)
    {
        require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Api.php';
        new Pay($app_config,$payMode_config);

    }


    /**
     * 统一付款接口
     *
     * @param $data  支付需要提供的参数
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedPay($data='')
    {
        if (empty($data)){
            $time=time();
            $data['out_trade_no'] ='111111'.$time;  //商户订单号，商户网站订单系统中唯一订单号，必填
            $data['subject']='测试订单'.$time;     //订单名称，必填
            $data['total_amount']=0.01;     //付款金额，必填
            $data['body']=$order_money='测试订单描述'.$time;     //商品描述，可空

        }


        $pay_data=[];
        $pay_data['appid']=\WxPayConfig::$APPID; //公众账号ID 必填
        $pay_data['mch_id']=\WxPayConfig::$MCHID; //商户号 必填
        //$pay_data['sub_appid']='';  //子商户公众账号ID
        //$pay_data['sub_mch_id']=''; //子商户号

        //$pay_data['device_info']='';  //设备号 非必须
        $pay_data['nonce_str']=H5Pay::getRandCode(32); //  随机字符串 必填

        $pay_data['body']=$data['body'];//商品描述  必填

        //$pay_data['detail']=$data['subject'];//商品详情  非必须
        //$pay_data['attach']=$data['subject'];//附加数据  非必须
        $pay_data['out_trade_no']=$data['out_trade_no'];//商户订单号  必填
        //$pay_data['fee_type']='CNY';//货币类型  非必须
        $pay_data['total_fee']=$data['total_amount']*100;//总金额  必填
        //$pay_data['product_id']=$order_data['product_id'];//商品ID  非必须



        $pay_data['spbill_create_ip']=$_SERVER['REMOTE_ADDR'];//终端IP  必填
        $pay_data['time_start']=date('YmdHis');//交易起始时间  必填
        //$pay_data['time_expire']=date('YmdHis');//交易结束时间  非必须
        //$pay_data['goods_tag']='WXG';//订单优惠标记  非必须

        //$pay_data['notify_url']=urldecode('http://webapp.lz4746.com/wxwechat/notify.php');//通知地址  必填
        $pay_data['notify_url']='http://webapp.lz4746.com/wxwechat/notify.php';//通知地址  必填

        $pay_data['trade_type']='MWEB';//交易类型  必填
        //$pay_data['limit_pay']='no_credit';//指定支付方式  非必须

        //$pay_data['openid']=$openId;//用户标识  必填
        // $pay_data['openid']='dddd';//用户标识  必填

        //$pay_data['sub_openid']='';//用户子标识  非必须

        //$pay_data['scene_info']='';//场景信息  非必须
        //$pay_data['sign_type']='MD5'; //签名类型 非必须 默认MD5
        //

        ksort($pay_data);

        $stringA='';
        foreach ($pay_data as $k=>$v){
            $stringA.= $k.'='.$v.'&';
        }
        $stringA.='key='.\WxPayConfig::$KEY;

        //第一次签名
        $string=strtoupper(md5($stringA));
        //print_r($string);


        unset($pay_data['key']);
        $pay_data['sign']=$string;//签名  必填

        $xml_dat="";
        $xml_dat.="<xml>".PHP_EOL;
        foreach ($pay_data as $k=>$v){
            $xml_dat.="<".$k."><![CDATA[".$v."]]></".$k.">".PHP_EOL;
        }
        $xml_dat.="</xml>";


        $return_data=H5Pay::httpPost($xml_dat); //执行curl

        //将返回的结果xml结果转成数组
        libxml_disable_entity_loader(true);
        $return_data_arr=simplexml_load_string($return_data,'SimpleXMLElement',LIBXML_NOCDATA);
        print_r($return_data_arr);die;
        //


        //重新签名
        /*$sign_three['appId']=self::APPID;
        $sign_three['timeStamp']=time();
        $sign_three['nonceStr']=$this->getRandCode(32);
        $sign_three['package']='prepay_id='.$return_data_arr->prepay_id;
        $sign_three['signType']='MD5';
        ksort($sign_three);


        $appId=self::APPID;
        $timeStamp=time();
        $nonceStr=$this->getRandCode(32);
        $package='prepay_id='.$return_data_arr->prepay_id;
        $signType='MD5';
        $stringThree="appId=$appId&nonceStr=$nonceStr&package=$package&signType=$signType&timeStamp=$timeStamp";
        $stringThree=$stringThree.'&key='.self::KEY;

        $sign3=strtoupper(md5($stringThree));

        //print_r($sign3);die;

        $r_dataa=[
            'timeStamp'=>$timeStamp,
            'nonceStr'=>$nonceStr,
            'package'=>$package,
            'signType'=>$signType,
            'paySign'=>$sign3,
        ];*/

        //print_r($r_dataa);die;
        return 'xxxx';

    }


    /**
     * 统一同步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台同步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnUrl($data)
    {
        print_r('开发中');die;
        $this->writeLog(json_encode($data,JSON_UNESCAPED_UNICODE));
        return $this->check($data);
    }

    /**
     * 统一异步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台异步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnNotify($data)
    {
        print_r('开发中');die;
        $this->writeLog(json_encode($data,JSON_UNESCAPED_UNICODE));
        return $this->check($data);
    }

    /**
     * 统一收单线下交易查询.
     *
     * @param \RjgcPay\查询条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本\
     */
    public function unifiedQuery($data)
    {
        print_r('开发中');die;
        return $response;
    }


    /**
     * 统一收单交易关闭接口.
     *
     * @param \RjgcPay\关闭条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedClose($data)
    {
        print_r('开发中');die;
        return $response;
    }

    /**
     * 统一收单交易退款接口.
     *
     * @param \RjgcPay\退款参数 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedRefund($data)
    {
        print_r('开发中');die;
        return $response;
    }


    /**
     * 统一收单交易退款查询.
     *
     * @param \RjgcPay\退款查询条件 $data
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedRefundQuery($data)
    {
        print_r('开发中');die;
        return $response;
    }


    public function unifiedDownloadurlQuery($data)
    {
        print_r('尚未实现');
        // TODO: Implement unifiedDownloadurlQuery() method.
    }

    /**
     * 验签方法
     *
     * @param $data 验签支付宝返回的信息，使用支付宝公钥
     */
    public function check($data)
    {
        print_r('开发中');die;
        return $result;
    }

    /**
     * 写入日志.
     *
     * @param $text
     */
    public function writeLog($text)
    {
        print_r('开发中');die;
        file_put_contents ( dirname ( __FILE__ ).DIRECTORY_SEPARATOR."./../../log.txt", date ( "Y-m-d H:i:s" ) . "  " . $text . "\r\n", FILE_APPEND );
    }

    private function _execute($request){
        print_r('开发中');die;
        return $result;
    }

}