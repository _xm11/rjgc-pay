<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\WxH5Pay;

/**
 * h5支付实现类
 * Class Editor
 * @package Rjgcimage\Gd
 */
class H5Pay
{
    const pay_unifiedorder='https://api.mch.weixin.qq.com/pay/unifiedorder';
    /**
     * 获取随机码
     */
    public static  function getRandCode($num=16){
        $array=array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','U','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','u','w','x','y','z',
            '1','2','3','4','5','6','7','8','9','0'
        );

        $temstr='';
        $max=count($array);
        for ($i=1;$i<=$num;$i++){
            $key=rand(0,$max-1);
            $temstr.=$array[$key];
        }
        return $temstr;
    }


    public static  function httpPost($post_data){

        //初使化init方法
        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, self::pay_unifiedorder); //指定URL
        curl_setopt($ch, CURLOPT_POST, 1); //声明使用POST方式来进行发送
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:text/xml; charset=utf-8"));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //设定请求后返回结果 获取的信息以文件流的形式返回
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); //发送什么数据呢

        //忽略证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);


        curl_setopt($ch, CURLOPT_HEADER, 0); //忽略header头信息
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置超时时间

        $output = curl_exec($ch); //发送请求
        curl_close($ch); //关闭curl

        //返回数据
        return $output;
    }
}