<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/27
 * Time: 11:15
 */

return [
    'pay_lavel'=>'WxH5Pay',  //支付标签（名称）
    'pay_path'=>'\RjgcPay\WxH5Pay\PayMode',  //支付全路径
    'pay_name'=>'微信H5支付',  //支付名称
    'pay_describe'=>'第三方手机浏览器', //支付描述


    'appid' => "",  //绑定支付的APPID（必须配置，开户邮件中可查看）
    'appsecret' => "",  //公众帐号secert（仅JSAPI支付的时候需要配置， 登录公众平台，进入开发者中心可设置）
    'mchid' => "",    //商户号（必须配置，开户邮件中可查看）
    'key' => "",  //商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）


    //退款用到证书，支付时不需要
    'sslcert_path' =>'../cert/apiclient_cert.pem',  //证书路径,注意应该填写绝对路径（仅退款、撤销订单时需要，可登录商户平台下载，
    'sslkey_path' => '../cert/apiclient_key.pem', //API证书下载地址：https://pay.weixin.qq.com/index.php/account/api_cert，下载之前需要安装商户操作证书）

    //异步通知地址
    //'notify_url' => "http://myali.lz4746.com/notify_url.php",
    'notify_url' => "	http://webapp.lz4746.com/wxwechat/notify.php",
];