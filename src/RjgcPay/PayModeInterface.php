<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:24
 */

namespace RjgcPay;

/**
 * 统一支付行为接口
 * Interface EditorInterface
 * @package Rjgcimage
 */
interface PayModeInterface
{

    /**
     * 统一付款接口.
     *
     * @param $data 支付需要提供的参数
     * @return mixed
     */
    public function unifiedPay($data);

    /**
     * 统一同步通知回调处理
     *
     * @param $data 支付后支付平台同步返回的数据
     * @return mixed
     */
    public function unifiedReturnUrl($data);


    /**
     * 统一异步通知回调处理
     *
     * @param $data 支付后支付平台异步返回的数据
     * @return mixed
     */
    public function unifiedReturnNotify($data);




    /**
     * 统一收单线下交易查询.
     *
     * @param $data 查询条件项
     * @return mixed
     */
    public function unifiedQuery($data);


    /**
     * 统一收单交易关闭接口
     *
     * @param $data 关闭条件项
     * @return mixed
     */
    public function unifiedClose($data);


    /**
     * 统一收单交易退款接口.
     *
     * @param $data 退款参数
     * @return mixed
     */
    public function unifiedRefund($data);


    /**
     * 统一收单交易退款查询
     *
     * @param $data 退款查询条件
     * @return mixed
     */
    public function unifiedRefundQuery($data);

    /**
     * 查询对账单下载地址
     *
     * @param $data 查询账单条件
     * @return mixed
     */
    public function unifiedDownloadurlQuery($data);


    /**
     * 验签方法
     *
     * @param $data 需要验签的数据
     * @return mixed
     */
    public function check($data);


    /**
     * 请确保项目文件有可写权限，不然打印不了日志。
     *
     * @param $text
     * @return mixed
     */
    public function writeLog($text);


}