<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/27
 * Time: 11:15
 */

return [
    'pay_lavel'=>'AliF2fPay', //支付标签（名称）
    'pay_path'=>'\RjgcPay\AliF2fPay\PayMode', //支付全路径
    'pay_name'=>'支付宝当面付支付', //支付名称
    'pay_describe'=>'线下当面付产品支持条码支付、扫码支付、声波支付。', //支付描述

    //应用ID,您的APPID。
    'app_id' => "2016082000296315",

    //商户私钥-由生成获取
    'merchant_private_key' => "MIIEogIBAAKCAQEAp9/SfHEzl5vGH4o8M8K02vP+5u7jMa6vW+YojRwjYUA0raDQlbrJSw/R8YBuU3R3Ef4BOZsWlRicwtlunxCDItHM5tf7nlgASn/GQvI1cEi+T9D/FCkxWzValcd9u+sGiZV5c91D9DXBvPNXJvIRfxc2th/devXFtMA+du+2Wdw4ZmTjl/MXFthaUZ1vNIphm/ZtOT6Msb4imGbmrEx0/59xiVUP6rE/hPDAYYcnWHpx8ZmLzGXW9G9WpTQdYPKttbinXehm7BhXN2r+u+B+edrdsK5cRPV7DxW68pS4b2xhQbzqD6N3/jihto5s8/ubd9Js8PanrOK0VbCZqC6UCQIDAQABAoIBAFUb6pDgq3FvrlCLOt5bFmnzTgYyksi7cXg8Bc8UtvM1I//rChT/L9J4YM35ePWilENw7ZoRAK/yXHeE2MqYCoP6rdFTKuQqZWtfwFpshZ85ohLBOLE1+SnusNBC5YICM7xJJdwSD8OeBW0QSd0a2LgAQuKu4RDXzbZHyONvwJlpdIl6CiCu05EN2gv1+AS+46CJmUFMmQwQgazGDfoCN02mcZNmt6J8VVnCGXrvZNDtDS9XvGYFeMffnIkbmKVr1dEcfVJk0xumnB4lM6N3BwQRJZ7N3cLdrKY0r5Kam1KdpOoHT9eZsSTMo9BT2USCslm300D+acOSBai1k7JDCckCgYEA0yP0i/LbRqKQ2T05I3ZNelrR4IBH8soNWqp6yNTUdE5SIdmtXzvUyCf53jHwiVNJLw2UqdoMZgQdOyzgNh5yDuyi10c1+gCJ9qHCP61BxFqxA1Q31loufMykkLPvrCqMlucx719UTYyZml2WPCBnSCjaFAdfPuf0zMjTWeBHeV8CgYEAy4qY/vjt7dNsO3XfdxHIKD3CNQT65daqlpvWXikzLFlcevUifzGI9nn0QR3YW0tOpYZOW4+uvh17ubyXYkw44QI8tBEx0hSj7+dV3BF7KG5GdKSFtm+0fW+g0DJE+DJAfUruY0BYtsp1lmcqEyUQ1BlP/V9+NWyuGWUNO0hSI5cCgYA4KE0H2A04aU6raRxEcVfEHlnKI3R/f/wzTDqiJfFoCdjQhgwV010R64gK8C9w5wUFDdCnqIY5sAhyT8pGuqxNieT560fQ+cm4HmgC9bsTVafVIdZEHfJVYTy3/BPYL+z6yf0/FJTgl2/XXeLylQ/DYpI2V4ueuKUkvplwQzXfcwKBgFxD+465MMG6MTnS+PV0rhLvy/VUyJcfdDzJb6dHbdcQuwdRz81wmvF/3uUVrlnUDVajRyniPAgaPVe40bbRbocxCGtnz/m3OYkqiJU1p2pwcqe1XJeSk3K5A2DdKL6JqQoiyWVcadqSdDM1mLENAE/lGYl1nDSDi3gfgpv48KMJAoGAa3mC7zlglgfPkzmhInwS9sCthIHIb+7H0IPC9lOfyAbQvNnl9tHjYeokqzgzIzQJHOkzV9kpgf7lvnz/5vvY4CkkQVH5aLnrilFHLGxIxbza/sYISmOwbyfyw0/abdzsOHOseNx+/mG+1fmiFY4hNq9NP/Y8ltFbZ6a5Xd3Dr1I=",

    //异步通知地址
    //'notify_url' => "http://myali.lz4746.com/notify_url.php",
    'notify_url' => "	http://pay.lz4746.com/return_notify.php",

    //同步跳转--回调地址
    'return_url' => "http://pay.lz4746.com/return_url.php",
    //'return_url' => "http://myali.lz4746.com/return_url.php",

    //编码格式
    'charset' => "UTF-8",

    //签名方式
    'sign_type'=>"RSA2",

    //支付宝网关
    'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do",  //沙箱环境
    //'gatewayUrl' => "https://openapi.alipay.com/gateway.do",    //正式环境

    'version'=>'1.0',

    //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。 --由创建应用获取
    'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoAlqOKNDUjcEg0SGBrhrGaJ3Jp1sYVWyQYxaKq+Qm//W/hZJdxXuPf4c0YAq3YJNfr4tTa1o6GZDeHL9MUDoeE+M+1zvH9VkcMWLXo9VMdbvrsrGCH0aIimPykdHHQrCyGp549mxam8QKefMky0ebkvRAm28N2a7hrWlrYb8mrnkK0txdqqbYr51DQW9p+yZ2GW9tDUhW270PJneEaKi4rsvrr+mSZJAB9FSGLgUKDZk+GuTPysWv2RxCufeB9EsCcysPxxOpEp12v+Zz+o7t0d4L/QdpW2xJBltcc1RbwWGhizpbo3c0kjV4UJC1aKknRyGFJ4MfoDk4IyhCzefiwIDAQAB",

    //是否开启调试
    'is_debug'=>true,

    //是否是页面接口，电脑网站支付是页面表单接口
    'is_page'=>true,
];