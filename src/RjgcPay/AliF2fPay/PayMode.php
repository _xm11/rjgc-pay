<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\AliF2fPay;

use RjgcPay\PayModeInterface;

/**
 * 支付宝支付处理类-刷卡支付
 *
 * @package RjgcPay\AliF2fPay
 */
class PayMode implements PayModeInterface
{

    /**
     * 支付构造函数
     * PayMode constructor.
     * @param $app_config  自定义配置信息(外部)
     * @param $payMode_config  特定支付配置信息（内部）
     * @throws \Exception
     */
    public function __construct($app_config,$payMode_config)
    {
        if (!empty($app_config)){
            $pay=new Pay($app_config);
            $this->is_debug=isset($app_config['is_debug'])?$app_config['is_debug']:false;

        }
        elseif (!empty($payMode_config)){
            $pay=new Pay($payMode_config);
            $this->is_debug=isset($payMode_config['is_debug'])?$payMode_config['is_debug']:false;
        }
        else{
            throw new \Exception('未定义应用端类型','444');
        }

        $this->pay=$pay;
    }

    public function unifiedPay($data)
    {
        // TODO: Implement unifiedPay() method.
    }

    public function unifiedReturnUrl($data)
    {
        // TODO: Implement unifiedReturnUrl() method.
    }

    public function unifiedReturnNotify($data)
    {
        // TODO: Implement unifiedReturnNotify() method.
    }

    public function unifiedQuery($data)
    {
        // TODO: Implement unifiedQuery() method.
    }

    public function unifiedClose($data)
    {
        // TODO: Implement unifiedClose() method.
    }

    public function unifiedRefund($data)
    {
        // TODO: Implement unifiedRefund() method.
    }

    public function unifiedRefundQuery($data)
    {
        // TODO: Implement unifiedRefundQuery() method.
    }

    public function unifiedDownloadurlQuery($data)
    {
        // TODO: Implement unifiedDownloadurlQuery() method.
    }

    public function check($data)
    {
        // TODO: Implement check() method.
    }

    public function writeLog($text)
    {
        // TODO: Implement writeLog() method.
    }


}