<?php

namespace RjgcPay;


use RjgcPay\AliF2fPay\PayMode as AliF2fPayPayMode;
use RjgcPay\AliPagePay\PayMode as AliPagePayPayMode;
use RjgcPay\AliWapPay\PayMode as AliWapPayPayMode;
use RjgcPay\AliAppPay\PayMode as AliAppPayPayMode;

use RjgcPay\UpacpWapPay\PayMode as UpacpWapPayPayMode;

use RjgcPay\WxAppPay\PayMode as WxAppPayPayMode;
use RjgcPay\WxF2fPay\PayMode as WxF2fPayPayMode;
use RjgcPay\WxH5Pay\PayMode as WxH5PayPayMode;
use RjgcPay\WxMiniPay\PayMode as WxMiniPayPayMode;
use RjgcPay\WxPcPay\PayMode as WxPcPayPayMode;
use RjgcPay\WxWeChatPay\PayMode as WxWeChatPayPayMode;

/**
 * 支付相关功能处理入口.
 *
 * Class Rjgc
 * @package Rjgcimage
 */

define('RJGC_DS', '/');
class RjgcPay
{
    private $DIR = __DIR__;


    /**
     * 校验支付方式是否存在.
     *
     * @param string $payLabel 支付类型名称
     * @return mixed
     * @throws \Exception
     */
    public  function payAbilityCheck($payLabel=''){
        if (empty($payLabel)){
            throw new \Exception('请指定一个支付方式','444');
        }

        $files=array();
        $dir_list=scandir($this->DIR);
        foreach($dir_list as $file){
            if($file!='..' && $file!='.'){

                $filename = $this->DIR . RJGC_DS . $file.'/config.php';

                if (is_dir($this->DIR) && file_exists($filename)) {
                    $file_data=include_once  $filename;
                    $files[] = $file_data;
                }
            }
        }

        foreach ($files as $k=>$v){
            if ($v['pay_lavel']==$payLabel){
                //print_r($this->DIR.RJGC_DS.$payLabel.RJGC_DS.'PayMode.php');die;
                //return $this->DIR.'/'.'AliPagepay'.'/'.'PayMode.php';
                //return $v['pay_path'];
                if(!array_key_exists('pay_path',$v)||empty($v['pay_path'])){
                    throw new \Exception('请填写必要的配置','444');
                }

                return $v;
            }
        }

        throw new \Exception('未定义支付方式','444');

    }


    /**
     * 创建支付方式.
     *
     * @param string $payLabel 支付方式标识（支付方式名称）
     * @param string $app_config 自定义支付方式配置
     * @return AliAppPayPayMode|AliF2fPayPayMode|AliPagePayPayMode|WxH5PayPayMode|WxMiniPayPayMode|WxPcPayPayMode|WxWeChatPayPayMode
     * @throws \Exception
     */
    public function createPayModel($payLabel='',$app_config=''){
        $payMode_config=$this->payAbilityCheck($payLabel);


        if ($payMode_config['pay_path']=='\RjgcPay\AliApppay\PayMode'){  //支付宝APP支付
            return new  AliAppPayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\AliF2fpay\PayMode'){  //支付宝面对面支付
            return new  AliF2fPayPayMode($app_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\AliPagepay\PayMode'){ //支付宝PC支付
            return new  AliPagePayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\AliWappay\PayMode'){  //支付宝手机网站支付
            return new  AliWapPayPayMode($app_config,$payMode_config);
        }


        elseif ($payMode_config['pay_path']=='\RjgcPay\UpacpWapPay\PayMode'){  //银联手机网站支付
            return new  UpacpWapPayPayMode($app_config,$payMode_config);
        }

        elseif ($payMode_config['pay_path']=='\RjgcPay\WxAppPay\PayMode'){  //微信APP支付
            return new  WxAppPayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\WxF2fPay\PayMode'){  //微信刷卡支付
            return new  WxF2fPayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\WxH5Pay\PayMode'){  //微信H5支付
            return new  WxH5PayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\WxMiniPay\PayMode'){  //微信小程序支付
            return new  WxMiniPayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\WxPcPay\PayMode'){  //微信扫码支付
            return new  WxPcPayPayMode($app_config,$payMode_config);
        }
        elseif ($payMode_config['pay_path']=='\RjgcPay\WxWeChatPay\PayMode'){  //微信公众号支付
            return new  WxWeChatPayPayMode($app_config,$payMode_config);
        }

        else{
            throw new \Exception('未添加支付处理方式','444');
        }


        /*include_once $payMode_path;
        $payMode=new \PayMode();
        print_r($payMode);die;*/
    }

    /**
     * 统一同步通知回调处理
     */
    public function unifiedReturnUrl(){

    }

    /**
     * 统一异步通知回调处理
     */
    public function unifiedReturnNotify(){

    }
}