<?php

namespace RjgcPay\WxPcPay\buildermodel;


class WxPayTradePagePayContentBuilder
{

    private $out_trade_no; //商户订单号，商户网站订单系统中唯一订单号，必填
    private $subject;  //订单名称，必填
    private $total_amount; //付款金额，必填
    private $body; //商品描述，必填
    private $product_id; //商品id
    private $goods_tag;//订单优惠标记
    private $model_type;//扫描模式

    public function __construct($data='')
    {

        if (!empty($data)){

            if(empty($data['out_trade_no'])||trim($data['out_trade_no'])==""){
                throw new \Exception("商户订单号!");
            }
            else{
                $this->out_trade_no = $data['out_trade_no'];
            }

            if(empty($data['subject'])||trim($data['subject'])==""){
                throw new \Exception("订单名称!");
            }
            else{
                $this->subject = $data['subject'];
            }

            if(empty($data['total_amount'])||trim($data['total_amount'])==""){
                throw new \Exception("付款金额!");
            }
            else{
                if (intval($data['total_amount']*100)<1){
                    throw new \Exception("付款金额不合法!");
                }
                else{
                    $this->total_amount = $data['total_amount'];
                }
            }


            if(empty($data['body'])||trim($data['body'])==""){
                throw new \Exception("商品描述!");
            }
            else{
                $this->body = $data['body'];
            }

            //商品id
            if(empty($data['product_id'])||trim($data['product_id'])==""){

            }
            else{
                $this->product_id = $data['product_id'];
            }

            if(empty($data['goods_tag'])||trim($data['goods_tag'])==""){

            }
            else{
                $this->goods_tag = $data['goods_tag'];
            }

            /*if(empty($data['model_type'])||trim($data['model_type'])==""){
                throw new \Exception("扫描模式必填!");
            }
            elseif (!in_array($data['model_type'],[1,2])){
                throw new \Exception("扫描模式可选值为1或2!");
            }
            else{

                $this->model_type = $data['body'];
            }*/

        }
        else{
            throw new \Exception("请提供支付参数!");
        }
    }

    /**
     * @return mixed
     */
    public function getOutTradeNo()
    {
        return $this->out_trade_no;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @return mixed
     */
    public function getGoodsTag()
    {
        return $this->goods_tag;
    }


}

?>