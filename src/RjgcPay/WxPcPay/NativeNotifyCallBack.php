<?php

namespace RjgcPay\WxPcPay;


require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Api.php';
require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Notify.php';
/**
 * 
 * 扫描支付异步回调(模式1)
 * @author widyhu
 *
 */
class NativeNotifyCallBack extends \WxPayNotify
{

    public function unifiedorder($openId, $product_id)
    {

        //根据商品id生成并获取订单信息
        require \WxPayConfig::$PC_PAY_ONE_GET_ORDER;

        //统一下单
        $input = new \WxPayUnifiedOrder();
        $input->SetBody("test");
        $input->SetAttach("test");
        $input->SetOut_trade_no(\WxPayConfig::$MCHID.date("YmdHis"));
        $input->SetTotal_fee("1");
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag("test");
        $input->SetNotify_url(\WxPayConfig::$NOTIFY_URL);
        $input->SetTrade_type("NATIVE");
        $input->SetOpenid($openId);
        $input->SetProduct_id($product_id);
        $result = \WxPayApi::unifiedOrder($input);
        //Log::DEBUG("unifiedorder:" . json_encode($result));
        return $result;
    }

    public function NotifyProcess($data, &$msg)
    {
        //echo "处理回调";
        //Log::DEBUG("call back:" . json_encode($data));
        if(!array_key_exists("openid", $data) ||
            !array_key_exists("product_id", $data))
        {
            $msg = "回调数据异常";
            return false;
        }

        $openid = $data["openid"];
        $product_id = $data["product_id"];

        //统一下单
        $result = $this->unifiedorder($openid, $product_id);
        if(!array_key_exists("appid", $result) ||
            !array_key_exists("mch_id", $result) ||
            !array_key_exists("prepay_id", $result))
        {
            $msg = "统一下单失败";
            return false;
        }

        $this->SetData("appid", $result["appid"]);
        $this->SetData("mch_id", $result["mch_id"]);
        $this->SetData("nonce_str", \WxPayApi::getNonceStr());
        $this->SetData("prepay_id", $result["prepay_id"]);
        $this->SetData("result_code", "SUCCESS");
        $this->SetData("err_code_des", "OK");
        return true;
    }
}