<?php

namespace RjgcPay\WxWeChatPay;


require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Api.php';
require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Notify.php';
/**
 * 
 * 扫描支付异步回调
 * @author widyhu
 *
 */
class PayNotifyCallBack extends \WxPayNotify
{

    //查询订单
    public function Queryorder($transaction_id)
    {
        $input = new \WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);
        $result = \WxPayApi::orderQuery($input);
        //Log::DEBUG("query:" . json_encode($result));
        if(array_key_exists("return_code", $result)
            && array_key_exists("result_code", $result)
            && $result["return_code"] == "SUCCESS"
            && $result["result_code"] == "SUCCESS")
        {
            return true;
        }
        return false;
    }

    //重写回调处理函数
    public function NotifyProcess($data, &$msg)
    {
        require APP_PATH.'wx_pay_notify.php';
        //Log::DEBUG("call back:" . json_encode($data));
        $notfiyOutput = array();

        if(!array_key_exists("transaction_id", $data)){
            $msg = "输入参数不正确";
            return false;
        }
        //查询订单，判断订单真实性
        if(!$this->Queryorder($data["transaction_id"])){
            $msg = "订单查询失败";
            return false;
        }


        
        return true;
    }
}