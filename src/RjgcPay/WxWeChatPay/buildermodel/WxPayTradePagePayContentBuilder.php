<?php

namespace RjgcPay\WxWeChatPay\buildermodel;


class WxPayTradePagePayContentBuilder
{

    private $out_trade_no; //商户订单号，商户网站订单系统中唯一订单号，必填
    private $subject;  //订单名称，必填
    private $total_amount; //付款金额，必填
    private $body; //商品描述，必填
    private $open_id; //用户标识 ，必填

    public function __construct($data='')
    {

        if (!empty($data)){

            if(empty($data['out_trade_no'])||trim($data['out_trade_no'])==""){
                throw new \Exception("商户订单号!");
            }
            else{
                $this->out_trade_no = $data['out_trade_no'];
            }

            if(empty($data['subject'])||trim($data['subject'])==""){
                throw new \Exception("订单名称!");
            }
            else{
                $this->subject = $data['subject'];
            }

            if(empty($data['total_amount'])||trim($data['total_amount'])==""){
                throw new \Exception("付款金额!");
            }
            else{
                if (intval($data['total_amount']*100)<1){
                    throw new \Exception("付款金额不合法!");
                }
                else{
                    $this->total_amount = $data['total_amount'];
                }
            }

            if(empty($data['body'])||trim($data['body'])==""){
                throw new \Exception("商品描述!");
            }
            else{
                $this->body = $data['body'];
            }

            if(empty($data['open_id'])||trim($data['open_id'])==""){

            }
            else{
                $this->open_id = $data['open_id'];
            }
        }
        else{
            throw new \Exception("请提供支付参数!");
        }
    }

    /**
     * @return mixed
     */
    public function getOutTradeNo()
    {
        return $this->out_trade_no;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getOpenId()
    {
        return $this->open_id;
    }



}

?>