<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\WxWeChatPay;
use RjgcPay\PayModeInterface;
use RjgcPay\WxWeChatPay\buildermodel\WxPayTradePagePayContentBuilder;

/**
 * 微信支付处理类-公众号支付
 *
 * @package RjgcPay\WxWeChatPay
 */
class PayMode implements PayModeInterface
{
    /**
     * 支付构造函数
     * PayMode constructor.
     * @param $app_config  自定义配置信息(外部)
     * @param $payMode_config  特定支付配置信息（内部）
     * @throws \Exception
     */
    public function __construct($app_config,$payMode_config)
    {
        require_once dirname(dirname ( __FILE__ )).'/lib/wx_sdk/lib/WxPay.Api.php';
        new Pay($app_config,$payMode_config);
    }


    public function weChatpay(){
        return new WeChatPay();
    }


    public function wePay(){
        return new WePay();
    }

    public function lucky(){
        return new LuckyMoney();
    }

    /**
     * 统一付款接口
     *
     * @param $data  支付需要提供的参数
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedPay($data='')
    {

       $wxPayTradePagePayContentBuilder=new WxPayTradePagePayContentBuilder($data);

        /*if(!isWeixinBrowser()){
            echo '请使用微信游览器！';die;
        }*/

        //①、获取用户openid
        $tools = new JsApiPay();
        //判断是否传入openID，若传则为使用外部授权机制，没传则使用内部授权机制
        if (empty($wxPayTradePagePayContentBuilder->getOpenId())){
            $openId = $tools->GetOpenid();
        }
        else{
            $openId=$wxPayTradePagePayContentBuilder->getOpenId();
        }

        //②、统一下单
        $input = new \WxPayUnifiedOrder();
        $input->SetBody($wxPayTradePagePayContentBuilder->getBody());
        $input->SetAttach($wxPayTradePagePayContentBuilder->getSubject());
        $input->SetOut_trade_no($wxPayTradePagePayContentBuilder->getOutTradeNo());
        $input->SetTotal_fee($wxPayTradePagePayContentBuilder->getTotalAmount()*100);
        //$input->SetTotal_fee(1);
        $input->SetTime_start(date("YmdHis"));
        //$input->SetTime_expire(date("YmdHis", time() + 1800));
        $input->SetGoods_tag("tp_wx_pay");
        $input->SetNotify_url('http://webapp.lz4746.com/wxwechat/notify.php');
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $result = \WxPayApi::unifiedOrder($input);

        if (isset($result['return_code'])&&$result['return_code']=='FAIL'){
            throw new \Exception($result['return_msg']);
        }

        $jsApiParameters = $tools->GetJsApiParameters($result);  //调用微信JS api 支付-参数信息
        $editAddress = $tools->GetEditAddressParameters();  //获取共享地址参数信息

        $return_data=[
            'jsApiParameters'=>$jsApiParameters,
            'editAddress'=>$editAddress,
        ];
        return $return_data;

    }


    /**
     * 统一同步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台同步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnUrl($data)
    {
        print_r('开发中');die;
        $this->writeLog(json_encode($data,JSON_UNESCAPED_UNICODE));
        return $this->check($data);
    }

    /**
     * 统一异步通知回调处理.
     *
     * @param \RjgcPay\支付后支付平台异步返回的数据 $data
     * @return bool|mixed
     */
    public function unifiedReturnNotify($data)
    {
        $notify=new PayNotifyCallBack();
        $notify->Handle(false);
    }

    /**
     * 统一收单线下交易查询.
     *
     * @param \RjgcPay\查询条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本\
     */
    public function unifiedQuery($data)
    {
        print_r('开发中');die;
        return $response;



    }


    /**
     * 统一收单交易关闭接口.
     *
     * @param \RjgcPay\关闭条件项 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedClose($data)
    {
        print_r('开发中');die;
        return $response;


    }

    /**
     * 统一收单交易退款接口.
     *
     * @param \RjgcPay\退款参数 $data
     * @return bool|\SimpleXMLElement[]|string|\提交表单HTML文本
     */
    public function unifiedRefund($data)
    {
        print_r('开发中');die;
        return $response;

    }


    /**
     * 统一收单交易退款查询.
     *
     * @param \RjgcPay\退款查询条件 $data
     * @return bool|mixed|\SimpleXMLElement|string|\提交表单HTML文本
     */
    public function unifiedRefundQuery($data)
    {
        print_r('开发中');die;
        return $response;
    }


    public function unifiedDownloadurlQuery($data)
    {
        print_r('尚未实现');
        // TODO: Implement unifiedDownloadurlQuery() method.
    }

    /**
     * 验签方法
     *
     * @param $data 验签支付宝返回的信息，使用支付宝公钥
     */
    public function check($data)
    {
        print_r('开发中');die;
        return $result;
    }

    /**
     * 写入日志.
     *
     * @param $text
     */
    public function writeLog($text)
    {
        print_r('开发中');die;
        file_put_contents ( dirname ( __FILE__ ).DIRECTORY_SEPARATOR."./../../log.txt", date ( "Y-m-d H:i:s" ) . "  " . $text . "\r\n", FILE_APPEND );
    }


    private function _execute($request){
        print_r('开发中');die;
        return $result;
    }

}