<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/2/25
 * Time: 15:09
 */

namespace RjgcPay\AliPagePay;

/**
 * 支付宝支付-PC网站支付
 *
 * @package RjgcPay\AliPagePay
 */
class Pay
{
    //支付宝网关地址
    protected $gateway_url = "https://openapi.alipay.com/gateway.do";

    //支付宝公钥
    protected $alipay_public_key;

    //商户私钥
    protected $private_key;

    //应用id
    protected $appid;

    //编码格式
    protected $charset = "UTF-8";

    protected $token = NULL;

    //返回数据格式
    protected $format = "json";

    protected $version='1.0';

    //签名方式
    protected $signtype = "RSA2";

    //同步跳转--回调地址
    protected $return_url;

    //异步通知地址
    protected $notify_url;

    function __construct($config){
        $this->gateway_url = $config['gatewayUrl'];
        $this->appid = $config['app_id'];
        $this->private_key = $config['merchant_private_key'];
        $this->alipay_public_key = $config['alipay_public_key'];
        $this->charset = $config['charset'];
        $this->signtype=$config['sign_type'];
        $this->version=$config['version'];
        $this->return_url=$config['return_url'];
        $this->notify_url=$config['notify_url'];

        if(empty($this->appid)||trim($this->appid)==""){
            throw new \Exception("appid should not be NULL!");
        }
        if(empty($this->private_key)||trim($this->private_key)==""){
            throw new \Exception("private_key should not be NULL!");
        }
        if(empty($this->alipay_public_key)||trim($this->alipay_public_key)==""){
            throw new \Exception("alipay_public_key should not be NULL!");
        }
        if(empty($this->charset)||trim($this->charset)==""){
            throw new \Exception("charset should not be NULL!");
        }
        if(empty($this->gateway_url)||trim($this->gateway_url)==""){
            throw new \Exception("gateway_url should not be NULL!");
        }
    }

    /**
     * @return string
     */
    public function getGatewayUrl()
    {
        return $this->gateway_url;
    }

    /**
     * @param string $gateway_url
     */
    public function setGatewayUrl($gateway_url)
    {
        $this->gateway_url = $gateway_url;
    }

    /**
     * @return mixed
     */
    public function getAlipayPublicKey()
    {
        return $this->alipay_public_key;
    }

    /**
     * @param mixed $alipay_public_key
     */
    public function setAlipayPublicKey($alipay_public_key)
    {
        $this->alipay_public_key = $alipay_public_key;
    }

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->private_key;
    }

    /**
     * @param mixed $private_key
     */
    public function setPrivateKey($private_key)
    {
        $this->private_key = $private_key;
    }

    /**
     * @return mixed
     */
    public function getAppid()
    {
        return $this->appid;
    }

    /**
     * @param mixed $appid
     */
    public function setAppid($appid)
    {
        $this->appid = $appid;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @return null
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param null $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getSigntype()
    {
        return $this->signtype;
    }

    /**
     * @param string $signtype
     */
    public function setSigntype($signtype)
    {
        $this->signtype = $signtype;
    }

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->return_url;
    }

    /**
     * @param mixed $return_url
     */
    public function setReturnUrl($return_url)
    {
        $this->return_url = $return_url;
    }

    /**
     * @return mixed
     */
    public function getNotifyUrl()
    {
        return $this->notify_url;
    }

    /**
     * @param mixed $notify_url
     */
    public function setNotifyUrl($notify_url)
    {
        $this->notify_url = $notify_url;
    }



}