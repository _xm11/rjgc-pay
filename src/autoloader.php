<?php
spl_autoload_register(function ($class) {
    if ( 0 === strpos( $class, 'RjgcPay' ) ) { // Autoload our packages only

        $base_dir = __DIR__ . '/';
        $file = str_replace('\\', '/', $base_dir . $class . '.php'); // Change \ to /

        $class_arr=explode('\\',$class);
       if (!in_array(end($class_arr),['WxPayApi','WxPayUnifiedOrder'])){
           require_once $file;
       }
    }
});